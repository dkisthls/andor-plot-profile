#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Created: May 14, 2018
# author: Chris Berst

import argparse
import csv
import re
import numpy as np
import matplotlib.pyplot as plt
import sys as sys
from decimal import Decimal 

word = 'Event0'
I_E0, E0_E1 = [], []
#t_vals = [[] for i in range(3)]   # comment this line in if you want to save individual time values
#n = 0    # use this line to look at only first part of log file

args = ''
#baseFileName = ''
plotTitle = ''
data = []
dataStartIndex = 0
xAxisLabel = ""
xAxisData = []
compareYAxisData = []
labels = []
testNum = []
stop = []
flush = []
config = []
queue = []
start = []
total = []
readout = []
framerate = []

#================================== reset() =================================---
#
def reset():
  global dataStartIndex
  global xAxisData
  global compareYAxisData 
  global labels
  global testNum
  global stop
  global flush
  global config
  global queue
  global start
  global total
  global readout
  global framerate

  dataStartIndex = 0
  del xAxisData[:]
  del compareYAxisData[:]
  del labels[:]
  del testNum[:]
  del stop[:]
  del flush[:]
  del config[:]
  del queue[:]
  del start[:]
  del total[:]
  del readout[:]
  del framerate[:]

#============================ plotConfigDetails() ==============================
#
def plotConfigDetails(baseFileName, shutterMode):
  global dataStartIndex
  global stop
  global flush
  global config
  global queue
  global start
  global total
  global plotTitle
  global xAxisLabel

  minVal = []
  minVal.append(min(stop))
  minVal.append(min(flush))
  minVal.append(min(config))
  minVal.append(min(queue))
  minVal.append(min(start))
  minVal.append(min(total))
  
  maxVal = []
  maxVal.append(max(stop))
  maxVal.append(max(flush))
  maxVal.append(max(config))
  maxVal.append(max(queue))
  maxVal.append(max(start))
  maxVal.append(max(total))
  
  avgVal = []
  avgVal.append(np.mean(stop))
  avgVal.append(np.mean(flush))
  avgVal.append(np.mean(config))
  avgVal.append(np.mean(queue))
  avgVal.append(np.mean(start))
  avgVal.append(np.mean(total))

#  newString = "{0:.6f}, {1:.6f}, {2:.6f}, {3:.6f}, {4:.6f}, {5:.6f}"
#  print('Min: ' + newString.format(minVal[0], minVal[1], minVal[2], minVal[3], minVal[4], minVal[5]))
#  print('Max: ' + newString.format(maxVal[0], maxVal[1], maxVal[2], maxVal[3], maxVal[4], maxVal[5]))
#  print('Avg: ' + newString.format(avgVal[0], avgVal[1], avgVal[2], avgVal[3], avgVal[4], avgVal[5]))

  fig1 = plt.figure(figsize=(30,15))
  fig1.suptitle(plotTitle, fontsize=18, fontweight='bold')
 
  plt.subplot(1, 1, 1)
  plt.tick_params(axis='both', which='major', labelsize=14)

  if baseFileName == "Exp" and args.xAxisUpperLimit:
    limits = (float(-0.001), float(args.xAxisUpperLimit))
    plt.xlim(limits)

  plt.plot(xAxisData, stop, label=labels[dataStartIndex])
  plt.plot(xAxisData, flush, label=labels[dataStartIndex+1])
  plt.plot(xAxisData, config, label=labels[dataStartIndex+2])
  plt.plot(xAxisData, queue, label=labels[dataStartIndex+3])
  plt.plot(xAxisData, start, label=labels[dataStartIndex+4])
  plt.plot(xAxisData, total, label=labels[dataStartIndex+5])
  plt.ylabel('Config Time (s)',fontweight="bold", fontsize=14)
  plt.xlabel(xAxisLabel,fontweight="bold", fontsize=14)
  plt.grid(True)
  plt.legend(bbox_to_anchor=(1.02, 1), loc=2, borderaxespad=0.)

  cell_text = []
  cell_text.append(['%1.6f' % val for val in minVal])
  cell_text.append(['%1.6f' % val for val in maxVal])
  cell_text.append(['%1.6f' % val for val in avgVal])

  row_labels = ('Min', 'Max', 'Avg')
  col_labels = []
  for i in range(0,6):
    col_labels.append(labels[dataStartIndex+i])
 
  lightGreen = (0.5, 0.8, 0.5)
  colors = [[(0.90, 0.90, 0.90) for c in range(6)] for r in range(3)]
  plt.table(cellText=cell_text,
            rowLabels=row_labels, rowColours=[lightGreen]*len(row_labels),
            colLabels=col_labels, colColours=[lightGreen]*len(col_labels),
            cellColours=colors,
            loc='top')
  
  fig1.show()
  fig1.savefig(shutterMode + "_" + baseFileName + ".details.png")

# End of plotConfigDetails()

#================================= plotTimes() =================================
#
def plotTimes(baseFileName, shutterMode):
  global plotTitle

#  newString = "{0:.6f}, {1:.6f}, {2:.6f}, {3:.6f}, {4:.6f}, {5:.6f}"
#  print('Min: ' + newString.format(minVal[0], minVal[1], minVal[2], minVal[3], minVal[4], minVal[5]))
#  print('Max: ' + newString.format(maxVal[0], maxVal[1], maxVal[2], maxVal[3], maxVal[4], maxVal[5]))
#  print('Avg: ' + newString.format(avgVal[0], avgVal[1], avgVal[2], avgVal[3], avgVal[4], avgVal[5]))

  row_labels = ('Min','Max')
  lightGreen = (0.5, 0.8, 0.5)

  fig1 = plt.figure(figsize=(25,15))
  fig1.suptitle(plotTitle, fontsize=18, fontweight='bold')

  # Framerate plot
  plt.subplot(3, 1, 1)

  if baseFileName == "Exp" and args.xAxisUpperLimit:
    limits = (float(-0.001), float(args.xAxisUpperLimit))
    plt.xlim(limits)

  plt.plot(xAxisData, framerate, label="Framerate")
  plt.ylabel('Frame Rate(Hz)',fontweight="bold", fontsize=14)
  plt.grid(True)

  cell_text = []
  cell_text.append(['%1.6f' % min(framerate)])
  cell_text.append(['%1.6f' % max(framerate)])

  col_labels = []
  col_labels.append('FrameRate(Hz)')
 
  colors = [[(0.90, 0.90, 0.90) for c in range(len(col_labels))] for r in range(len(row_labels))]
  plt.table(cellText=cell_text,
            colWidths=[0.1 for x in col_labels],
            rowLabels=row_labels, rowColours=[lightGreen]*len(row_labels),
            colLabels=col_labels, colColours=[lightGreen]*len(col_labels),
            cellColours=colors, cellLoc='center',
            loc='bottom right')
 
  # Readout Time plot 
  plt.subplot(3, 1, 2)

  if baseFileName == "Exp" and args.xAxisUpperLimit:
    limits = (float(-0.001), float(args.xAxisUpperLimit))
    plt.xlim(limits)

  plt.plot(xAxisData, readout, label="Readout")
  plt.ylabel('Readout Time(s)',fontweight="bold", fontsize=14)
  plt.grid(True)

  cell_text = []
  cell_text.append(['%1.6f' % min(readout)])
  cell_text.append(['%1.6f' % max(readout)])

  col_labels = []
  col_labels.append('Readout(s)')
 
  colors = [[(0.90, 0.90, 0.90) for c in range(len(col_labels))] for r in range(len(row_labels))]
  plt.table(cellText=cell_text,
            colWidths=[0.1 for x in col_labels],
            rowLabels=row_labels, rowColours=[lightGreen]*len(row_labels),
            colLabels=col_labels, colColours=[lightGreen]*len(col_labels),
            cellColours=colors, cellLoc='center',
            loc='bottom right')
  
  # Config Time plot
  plt.subplot(3, 1, 3)

  if baseFileName == "Exp" and args.xAxisUpperLimit:
    limits = (float(-0.001), float(args.xAxisUpperLimit))
    plt.xlim(limits)

  plt.plot(xAxisData, total, label="Config")
  plt.ylabel('Config Time (s)',fontweight="bold", fontsize=14)
  plt.xlabel(xAxisLabel,fontweight="bold", fontsize=14)
  plt.grid(True)

  cell_text = []
  cell_text.append(['%1.6f' % min(total)])
  cell_text.append(['%1.6f' % max(total)])

  col_labels = []
  col_labels.append('Config(s)')
 
  colors = [[(0.90, 0.90, 0.90) for c in range(len(col_labels))] for r in range(len(row_labels))]
  plt.table(cellText=cell_text,
            colWidths=[0.1 for x in col_labels],
            rowLabels=row_labels, rowColours=[lightGreen]*len(row_labels),
            colLabels=col_labels, colColours=[lightGreen]*len(col_labels),
            cellColours=colors, cellLoc='center',
            loc='bottom right')
  
  fig1.show()
  fig1.savefig(shutterMode + "_" + baseFileName + ".times.png")

# End of plotTimes()

#============================= plotComparison() =================================
#
def plotComparison(coreName, shutterMode, baseFileName):

  fig1 = plt.figure(figsize=(25,15))
  fig1.suptitle("Comparison: " + plotTitle, fontsize=18, fontweight='bold')

  # Comparison plot
  plt.subplot(1, 1, 1)

  if coreName == "Exp" and args.xAxisUpperLimit:
    limits = (float(-0.001), float(args.xAxisUpperLimit))
    plt.xlim(limits)

  # The comparison data set and this one may not have the same number of samples
  # Limit the number of samples to the minimum of the two sets.
  maxElements = min(len(total), len(compareYAxisData))

  plt.plot(xAxisData[:maxElements], total[:maxElements], label="Current")
  plt.plot(xAxisData[:maxElements], compareYAxisData[:maxElements], label="Previous")
  plt.ylabel('Config Time (s)',fontweight="bold", fontsize=14)
  plt.xlabel(xAxisLabel,fontweight="bold", fontsize=14)
  plt.grid(True)
  plt.legend(bbox_to_anchor=(1.02, 1), loc=2, borderaxespad=0.)

  fig1.show()
  fig1.savefig(shutterMode + "_" + baseFileName+".compare.png")

# End of plotComparison()


#=============================== processFile() =================================
#
def processFile(coreName, shutterModeIdx):
  global dataStartIndex
  global xAxisData
  global compareYAxisData 
  global labels
  global testNum
  global stop
  global flush
  global config
  global queue
  global start
  global total
  global readout
  global framerate
  global plotTitle
  global xAxisLabel

  dataFound = False
  xAxisIndex = 0
  xAxisType = "int"
  shutterMode = args.shutterModes[shutterModeIdx]
  print("processFile: shutterMode={}".format(shutterMode))

  shutterModeFound = False
  reset()
  with open(args.inputFile, 'r') as csvfile:
    fileReader = csv.reader(csvfile, delimiter=',', quotechar='|')

    # Find the start of the PASS/ShutterMode of interest...
    for row in fileReader:
      if len(row) > 0 and "PASS" in row[0] and shutterMode in row[0]:
        shutterModeFound = True
        break

    if not shutterModeFound:
      print("Unable to locate results for shutterMode=\"{}\"".format(shutterMode))
      return

    # Start processing test data...
    reset()
    for row in fileReader:
      if len(row) == 0:                 # Skip empty lines
        continue
      elif "=============" in row[0]:   # Test separator
        # Read next row. It contains the test name text we need.
        nextRow = next(fileReader)
        if len(nextRow) == 0:           # Skip empty lines
          continue

        tmp1 = nextRow[0]               # First element determines plot title

        # for backward compatiblity with Exp test that lists the RowReadTime
        # right after the "============" test divider
        if tmp1.find("RowReadTime") != -1:
          tmp1 = next(fileReader)[0]

        # Check for "PASS" which may follow the current "PASS" being plotted
        # or "Testing" which is only in the last line. If either is found then 
        # we are done
        if tmp1.find("Testing") != -1 or tmp1.find("PASS") != -1:
          break

        # Read next row. This may contain info for binning configuration
        # tmp2 = next(fileReader)[0]
        # if len(tmp2) == 0:              # Skip empty lines
        #   continue
        
        # Set baseFileName and labels...
        # Aoi
#        if tmp1.find("AOI ") != -1:
        if coreName == "Aoi" or coreName.find("AoiExp") != -1:
          plotTitle = tmp1

          if plotTitle.find("AOI Width") != -1:
            xAxisLabel = "AOI Width (pixels)"
            baseFileName = coreName + "_Width"
          elif plotTitle.find("AOI Height") != -1:
            xAxisLabel = "AOI Height (pixels)"
            baseFileName = coreName + "_Height"
          elif plotTitle.find("AOI Left") != -1:
            xAxisLabel = "AOI Left (pixels)"
            baseFileName = coreName + "_Left"
          elif plotTitle.find("AOI Top") != -1:
            xAxisLabel = "AOI Top (pixels)"
            baseFileName = coreName + "_Top"

        elif coreName == "Bin":
          xAxisLabel = "Test #"

          if tmp1.find("AOIHBin") != -1:
            plotTitle = "AOIHBin/AOIVBin"
            baseFileName = coreName + "_AOIxBin"
          elif tmp1.find("AOIBinning") != -1:
            plotTitle = "AOIBinning"
            baseFileName = coreName + "_AOIBinning"
          else:
            print("TERMINATING - Cannot determine binning type for Bin!")
            exit(0)

        elif coreName.find("BinExp") != -1:
          xAxisLabel = "Test #"

          tmp2 = next(fileReader)[0]
          binTypeFound = False
          for i in range(0,2):
            if tmp2.find("AOIHBin") != -1:
              binTypeFound = True
              plotTitle = "Binning(using AOIxBin) and Exposure Time"
              baseFileName = coreName + "_AOIxBin"
            elif tmp2.find("AOIBinning") != -1:
              binTypeFound = True
              plotTitle = "Binning(using AOIBinning) and Exposure Time"
              baseFileName = coreName + "_AOIBinning"
            if not binTypeFound :
              tmp2 = next(fileReader)[0]

          if not binTypeFound:
            print("TERMINATING - Cannot determine binning type for BinExp!")
            exit(0)

        elif coreName == "Exp":
          plotTitle = "Exposure Time"
          xAxisLabel = "Exposure Time (s)"
          baseFileName = coreName
 

        # AOI and Binning(using AOIHBin & AOIVBin)
        # elif tmp1.find("AOIHBin") != -1:
        #   plotTitle = tmp1
        #   xAxisLabel = "Test #"
        #   baseFileName = "Bin(AOIxBin)"

        # # AOI and Binning(using AOIBinning)
        # elif tmp1.find("AOIBinning") != -1:
        #   plotTitle = tmp1
        #   xAxisLabel = "Test #"
        #   baseFileName = "Bin(AOIBinning)"
   
        # # Exposure Time and Binning (using AOIHBin & AOIVBin)
        # elif tmp1.find("Exposure") != -1 and tmp2.find("AOIHBin") != -1:
        #   plotTitle = "Exposure Time and Binning(using AOIxBin)"
        #   xAxisLabel = "Test #"
        #   baseFileName = "ExpBin(AOIxBin)"
   
        # # Exposure Time and Binning (using AOIBinning)
        # elif tmp1.find("Exposure") != -1 and tmp2.find("AOIBinning") != -1:
        #   plotTitle = "Exposure Time and Binning(using AOIBinning)"
        #   xAxisLabel = "Test #"
        #   baseFileName = "ExpBin(AOIBinning)"

        # # Exposure Time Only
        # elif tmp1.find("Exposure") != -1 and tmp2.find("AOIBinning") == -1:
        #   plotTitle = "Exposure Time"
        #   xAxisLabel = "Exposure Time(s)"
        #   baseFileName = "Exp"

  #        plotTitle = next(fileReader)[0]
  #      print(plotTitle)
        plotTitle = plotTitle + ", ElectronicShutteringMode=\"" + shutterMode + "\""
        print( "Generating plot: " + plotTitle + ", baseFileName=" + baseFileName)

      elif "Test Range" in row[0]:
        # Skip lines that define test ranges
        continue

      elif "Test" in row[0]:
        dataFound = True
        reset()
        
        currIndex = 0
        for item in row:
          strippedItem = item.strip()
          labels.append(strippedItem)
            
          # if "Stop" in strippedItem:
          #   dataStartIndex = currIndex;
          # elif baseFileName == "Aoi_Width" and "Wdth" in strippedItem:
          #   xAxisIndex = currIndex;
          # elif baseFileName == "Aoi_Height" and "Hght" in strippedItem:
          #   xAxisIndex = currIndex;
          # elif baseFileName == "Aoi_Left" and "Left" in strippedItem:
          #   xAxisIndex = currIndex;
          # elif baseFileName == "Aoi_Top" and "Top" in strippedItem:
          #   xAxisIndex = currIndex;
          # elif baseFileName.find("Bin(AOI") != -1 and "Test" in strippedItem:
          #   xAxisIndex = currIndex;
          # elif baseFileName.find("ExpBin") != -1 and "Test" in strippedItem:
          #   xAxisIndex = currIndex;
          # elif baseFileName == "Exp" and "expTime" in strippedItem:
          #   xAxisIndex = currIndex;
          #   xAxisType = "float"

          if "Stop" in strippedItem:
            dataStartIndex = currIndex;
          elif "Width" in baseFileName and "Wdth" in strippedItem:
            xAxisIndex = currIndex;
          elif "Height" in baseFileName and "Hght" in strippedItem:
            xAxisIndex = currIndex;
          elif "Left" in baseFileName and "Left" in strippedItem:
            xAxisIndex = currIndex;
          elif "Top" in baseFileName and "Top" in strippedItem:
            xAxisIndex = currIndex;
          elif "Bin" in baseFileName and "Test" in strippedItem:
            xAxisIndex = currIndex;
          elif "Exp" == coreName and "expTime" in strippedItem:
            xAxisIndex = currIndex;
            xAxisType = "float"

          currIndex += 1

  #      print('Header: ' + ', '.join(labels))

      elif "  Flush  " in row:
        dataFound = False
        plotConfigDetails(baseFileName, shutterMode)
        plotTimes(baseFileName, shutterMode)

        # If requested, generate a comparison plot...
        if args.pathToCompare:
          with open(args.pathToCompare + '/compare_' + shutterMode + "_" + baseFileName + ".txt", 'r') as compareCsvfile:
            compareFileReader = csv.DictReader(compareCsvfile, delimiter=',', quotechar='|')
            for compareRow in compareFileReader:
              compareYAxisData.append( float(compareRow['yAxis']) )

          plotComparison(coreName, shutterMode, baseFileName)

        # Generate a comparison file from the current data set containing the X/Y 
        # axis data of the current plot.
        outfile = open('compare_' + shutterMode + '_' + baseFileName + ".txt", 'w')
        with outfile:
          fileWriter = csv.writer(outfile, delimiter=' ', quotechar='|')

          myFields = ['xAxis', 'yAxis']
          fileWriter = csv.DictWriter(outfile, fieldnames=myFields) #delimiter=',', quotechar='|')
          fileWriter.writeheader()

          for i in range(len(total)):
            fileWriter.writerow({'xAxis' : xAxisData[i],'yAxis' : total[i]})

  #      input( "Press return to exit..." )
  #      break

      elif dataFound == True:
        tmpData = []
        for item in row:
          tmpData.append(item.strip())

  #      print('Data: ' + ', '.join(tmpData))

        testNum.append(int(tmpData[0]))
        if xAxisType == "int":
          xAxisData.append(int(tmpData[xAxisIndex]))
        elif xAxisType == "float":
          xAxisData.append(float(tmpData[xAxisIndex]))
        else:
          print("TERMINATING - Unknown X-axis Data Type!")
          break

        stop.append(float(tmpData[dataStartIndex]))
        flush.append(float(tmpData[dataStartIndex+1]))
        config.append(float(tmpData[dataStartIndex+2]))
        queue.append(float(tmpData[dataStartIndex+3]))
        start.append(float(tmpData[dataStartIndex+4]))
        total.append(float(tmpData[dataStartIndex+5]))
        readout.append(float(tmpData[dataStartIndex+6]))
        framerate.append(float(tmpData[dataStartIndex+7]))

  #      newString = "Dat2: {}, {}, {}, {}, {}, {}, {}, {}, {}"
  #      i = len(testNum) - 1
  #      print(newString.format(testNum[i], stop[i], flush[i], config[i], queue[i], start[i], 
  #                             total[i], readout[i], framerate[i]))
   


#================================== open() =====================================
#
# NOTE: argv[1] = Input Filename
#       argv[2] = Use upper limit: true/false
#                 true: argv[3] = Upper limit (used with Exp plots only)
#                 false: no upper limit, plot full range
#       argv[3] = if argv[2] == true, Upper limit (used with Exp plots only)
#                 if argv[2] == false, Comparison plot: true/false
#       argv[4] = if argv[3]

parser = argparse.ArgumentParser(description='Plot Andor Balor profile data.')

parser.add_argument("inputFile", action="store", 
                    help="input filename to plot")
parser.add_argument("-l", "--limit", action="store", dest="xAxisUpperLimit",
                    help="upper limit(used with Exp plots only)")
parser.add_argument("-s", "--shutterModes", action="store", dest="shutterModes",
                    nargs="*", default=["Global","Rolling"],
                    help="ElectronicShutteringMode to plot")
parser.add_argument("-c", "--compare", action="store", dest="pathToCompare",
                    help="plot comparison with named path")

args = parser.parse_args()
# Take inpupt filename of form 'profile_XYZ.results.txt' and get the XYZ part
coreName = args.inputFile.partition('_')[2].partition('.')[0]

for s in range(len(args.shutterModes)):
  print("\nProcessing: " + args.inputFile + " for " + args.shutterModes[s])
  processFile( coreName, s )

# with open(args.inputFile, 'r') as csvfile:
#   fileReader = csv.reader(csvfile, delimiter=',', quotechar='|')

#   next(fileReader)
#   next(fileReader)
#   next(fileReader)

#   passFound = False
#   for row in fileReader:
#     if len(row) > 0 and "PASS" in row[0] and elecShutterMode in row[0]:
#       print(row)
#       break
  

#   for row in fileReader:
#     print(row)
#     exit(0)

#     if len(row) > 0  and "======" in row[0]:
#       tmp1 = next(fileReader)[0]

#       # Check for "Testing" which is only in the last line. If found then 
#       # we are done
#       if tmp1.find("Testing") != -1:
#         break

#       tmp2 = next(fileReader)[0]
      
#       # AOI
#       if tmp1.find("AOI ") != -1:
#         plotTitle = tmp1

#         if plotTitle.find("AOI Width") != -1:
#           xAxisLabel = "Pixels"
#           baseFileName = "AOI_Width"
#         elif plotTitle.find("AOI Height") != -1:
#           xAxisLabel = "Pixels"
#           baseFileName = "AOI_Height"
#         elif plotTitle.find("AOI Left") != -1:
#           xAxisLabel = "Pixels"
#           baseFileName = "AOI_Left"
#         elif plotTitle.find("AOI Top") != -1:
#           xAxisLabel = "Pixels"
#           baseFileName = "AOI_Top"
        
#       # AOI and Binning(using AOIHBin & AOIVBin)
#       elif tmp1.find("AOIHBin") != -1:
#         plotTitle = tmp1
#         xAxisLabel = "Test #"
#         baseFileName = "Bin(AOIxBin)"

#       # AOI and Binning(using AOIBinning)
#       elif tmp1.find("AOIBinning") != -1:
#         plotTitle = tmp1
#         xAxisLabel = "Test #"
#         baseFileName = "Bin(AOIBinning)"
 
#       # Exposure Time and Binning (using AOIHBin & AOIVBin)
#       elif tmp1.find("Exposure") != -1 and tmp2.find("AOIHBin") != -1:
#         plotTitle = "Exposure Time and Binning(using AOIxBin)"
#         xAxisLabel = "Test #"
#         baseFileName = "ExpBin(AOIxBin)"
 
#       # Exposure Time and Binning (using AOIBinning)
#       elif tmp1.find("Exposure") != -1 and tmp2.find("AOIBinning") != -1:
#         plotTitle = "Exposure Time and Binning(using AOIBinning)"
#         xAxisLabel = "Test #"
#         baseFileName = "ExpBin(AOIBinning)"

#       # Exposure Time Only
#       elif tmp1.find("Exposure") != -1 and tmp2.find("AOIBinning") == -1:
#         plotTitle = "Exposure Time"
#         xAxisLabel = "Exposure Time(s)"
#         baseFileName = "Exp"

# #        plotTitle = next(fileReader)[0]
# #      print(plotTitle)

#     elif "Test" in row:
#       reset()
      
#       dataFound = True
#       currIndex = 0
#       for item in row:
#         strippedItem = item.strip()
#         labels.append(strippedItem)
          
#         if "Stop" in strippedItem:
#           dataStartIndex = currIndex;
#         elif baseFileName == "AOI_Width" and "Wdth" in strippedItem:
#           xAxisIndex = currIndex;
#         elif baseFileName == "AOI_Height" and "Hght" in strippedItem:
#           xAxisIndex = currIndex;
#         elif baseFileName == "AOI_Left" and "Left" in strippedItem:
#           xAxisIndex = currIndex;
#         elif baseFileName == "AOI_Top" and "Top" in strippedItem:
#           xAxisIndex = currIndex;
#         elif baseFileName.find("Bin(AOI") != -1 and "Test" in strippedItem:
#           xAxisIndex = currIndex;
#         elif baseFileName.find("ExpBin") != -1 and "Test" in strippedItem:
#           xAxisIndex = currIndex;
#         elif baseFileName == "Exp" and "expTime" in strippedItem:
#           xAxisIndex = currIndex;
#           xAxisType = "float"

#         currIndex += 1

# #      print('Header: ' + ', '.join(labels))

#     elif "  Flush  " in row:
#       dataFound = False
#       plotConfigDetails()
#       plotTimes()

#       # If requested, generate a comparison plot...
#       if args.pathToCompare:
#         with open(args.pathToCompare + '/compare_' + baseFileName + ".txt", 'r') as compareCsvfile:
#           compareFileReader = csv.DictReader(compareCsvfile, delimiter=',', quotechar='|')
#           for compareRow in compareFileReader:
#             compareYAxisData.append( float(compareRow['yAxis']) )

#         plotComparison()

#       # Generate a comparison file from the current data set containing the X/Y 
#       # axis data of the current plot.
#       outfile = open('compare_' + baseFileName + ".txt", 'w')
#       with outfile:
#         fileWriter = csv.writer(outfile, delimiter=' ', quotechar='|')

#         myFields = ['xAxis', 'yAxis']
#         fileWriter = csv.DictWriter(outfile, fieldnames=myFields) #delimiter=',', quotechar='|')
#         fileWriter.writeheader()

#         for i in range(len(total)):
#           fileWriter.writerow({'xAxis' : xAxisData[i],'yAxis' : total[i]})

# #      input( "Press return to exit..." )
# #      break

#     elif dataFound == True:
#       tmpData = []
#       for item in row:
#         tmpData.append(item.strip())

# #      print('Data: ' + ', '.join(tmpData))

#       testNum.append(int(tmpData[0]))
#       if xAxisType == "int":
#         xAxisData.append(int(tmpData[xAxisIndex]))
#       elif xAxisType == "float":
#         xAxisData.append(float(tmpData[xAxisIndex]))
#       else:
#         print("UNKNOWN X-AXIS DATA TYPE - TERMINATING!")
#         break

#       stop.append(float(tmpData[dataStartIndex]))
#       flush.append(float(tmpData[dataStartIndex+1]))
#       config.append(float(tmpData[dataStartIndex+2]))
#       queue.append(float(tmpData[dataStartIndex+3]))
#       start.append(float(tmpData[dataStartIndex+4]))
#       total.append(float(tmpData[dataStartIndex+5]))
#       readout.append(float(tmpData[dataStartIndex+6]))
#       framerate.append(float(tmpData[dataStartIndex+7]))

# #      newString = "Dat2: {}, {}, {}, {}, {}, {}, {}, {}, {}"
# #      i = len(testNum) - 1
# #      print(newString.format(testNum[i], stop[i], flush[i], config[i], queue[i], start[i], 
# #                             total[i], readout[i], framerate[i]))
 
# End of open()

